///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief EE205 - Lab07d - Animal Farm 1
///
/// This module updates cats in the database
///
/// @file updateCats.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 25_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef UPDATECATS_H
#define UPDATECATS_H

#include <string.h>

#include "catDatabase.h"
#include "addCats.h"
#include "validate.h"

bool fixCat(const size_t index);

bool updateCatName(const size_t index, const char *newName);

bool updateCatWeight(const size_t index, const float newWeight);

bool updateCatCollar1(const size_t index, const enum Color newColor);

bool updateCatCollar2(const size_t index, const enum Color newColor);

bool updateLicense(const size_t index, const unsigned long long int newLicense);

#endif