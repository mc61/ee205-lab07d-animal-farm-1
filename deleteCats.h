///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief EE205 - Lab07d - Animal Farm 1
///
/// This module deletes cats from the database
///
/// @file deleteCats.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 25_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef DELETECATS_H
#define DELETECATS_H

#include "catDatabase.h"
#include "validate.h"

void deleteAllCats();

int deleteCat(const size_t index);

#endif