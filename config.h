///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief EE205 - Lab07d - Animal Farm 1
///
/// This module is used throughout the .c files in the program to control various configuration parameters
///
/// @file config.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 25_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#define DEBUG_ENABLE

#define PROGRAM_NAME "Animal Farm 1"

#define BAD_CAT -1