///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief EE205 - Lab07d - Animal Farm 1
///
/// This module updates cats in the database
///
/// @file validate.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 25_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef VALIDATE_H
#define VALIDATE_H

#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "catDatabase.h"

bool nameIsValid(const char *name);

bool weightIsValid(const float weight);

bool indexIsValid(const size_t index);

bool collarColorsAreValid(const enum Color collar1, const enum Color collar2);

bool licenseIsValid(const unsigned long long licenseNum);

#endif