///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief EE205 - Lab07d - Animal Farm 1
///
/// This module adds cats to the database
///
/// @file addCats.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 25_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef ADDCATS_H
#define ADDCATS_H

#include "catDatabase.h"
#include "validate.h"

int addCat(const char *name,
           const enum Gender gender,
           const enum Breed breed,
           const bool isFixed,
           const float weight,
           const enum Color collarColor1,
           const enum Color collarColor2,
           const unsigned long long license);

#endif