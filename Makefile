###############################################################################
###         University of Hawaii, College of Engineering
### @brief  Lab 05d - Animalfarm 0 - EE 205 - Spr 2022
###
### @file Makefile
### @version 1.0
###
### Makes compiling and linking EZ-Peezy Lemon Squeezy
###
### @author Caleb Mueller <mc61@hawaii.edu>
### @date   25_FEB_2022
###############################################################################
CC = gcc
CFLAGS = -g -Wall -Wextra
#CFLAGS = -g -Wall -Wextra -DDEBUG_ENABLE #<--Another way to enable debug mode

TARGET = animalFarm

all: $(TARGET)

addCats.o: addCats.c addCats.h catDatabase.h validate.h
	$(CC) $(CFLAGS) -c addCats.c

catDatabase.o: catDatabase.c catDatabase.h 
	$(CC) $(CFLAGS) -c catDatabase.c

deleteCats.o: deleteCats.c deleteCats.h catDatabase.h validate.h
	$(CC) $(CFLAGS) -c deleteCats.c

reportCats.o: reportCats.c reportCats.h catDatabase.h validate.h
	$(CC) $(CFLAGS) -c reportCats.c

updateCats.o: updateCats.c updateCats.h catDatabase.h addCats.h validate.h
	$(CC) $(CFLAGS) -c updateCats.c

validate.o: validate.c validate.h catDatabase.h
	$(CC) $(CFLAGS) -c validate.c

main.o: main.c addCats.h catDatabase.h deleteCats.h reportCats.h updateCats.h validate.h
	$(CC) $(CFLAGS) -c main.c

animalFarm: main.o addCats.o catDatabase.o deleteCats.o reportCats.o updateCats.o validate.o
	$(CC) $(CFLAGS) -o $(TARGET) main.o addCats.o catDatabase.o deleteCats.o reportCats.o updateCats.o validate.o

clean:
	rm -f $(TARGET) *.o

test: animalFarm
	./$(TARGET)
