///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief EE205 - Lab07d - Animal Farm 1
///
/// This module defines all of the enums, the arrays, the maximum size of the array and
/// the number of cats in the array.
///
/// @file catDatabase.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 25_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef CATDATABASE_H
#define CATDATABASE_H

#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include <time.h> //For Birthday
#include <locale.h>

#define MAX_CATS 1024
#define MAX_NAME_LEN 50
#define MAX_CAT_WEIGHT 1000

extern size_t currentNumberOfCats;

enum Gender
{
    UNKNOWN_GENDER,
    MALE,
    FEMALE
};

enum Breed
{
    UNKNOWN_BREED,
    MAINE_COON,
    MANX,
    SHORTHAIR,
    PERSIAN,
    SPHYNX
};

enum Color
{
    UNASSIGNED,
    BLACK,
    WHITE,
    RED,
    BLUE,
    GREEN,
    PINK
};

struct Cat
{
    char name[MAX_NAME_LEN];
    enum Gender gender;
    enum Breed breed;
    float weight;
    bool isFixed;
    enum Color collarColor1;
    enum Color collarColor2;
    unsigned long long license;
};

extern struct Cat cats[];

void initializeDatabase();

char *genderLiteral(const enum Gender gender);

char *breedLiteral(const enum Breed breed);

char *colorLiteral(const enum Color color);

#endif
