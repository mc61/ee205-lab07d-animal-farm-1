///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief EE205 - Lab07d - Animal Farm 1
///
/// This module deletes cats from the database
///
/// @file deleteCats.c
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 25_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#include "deleteCats.h"
#include "catDatabase.h"
#include "config.h"

void deleteAllCats()
{
    initializeDatabase();
}

int deleteCat(const size_t index)
{
    if (indexIsValid(index) == false)
    {
        fprintf(stderr, "%s Error: deleteCat was not able to execute\n", PROGRAM_NAME);
        return BAD_CAT;
    }

    cats[index] = cats[currentNumberOfCats - 1];

    memset(&cats[currentNumberOfCats], 0, sizeof(struct Cat));

    currentNumberOfCats--;

    return 1;
}