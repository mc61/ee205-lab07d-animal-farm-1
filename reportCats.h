///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// @brief EE205 - Lab07d - Animal Farm 1
///
/// This module finds cats and prints the entire database
///
/// @file reportCats.h
/// @version 1.0
///
/// @author Caleb Mueller <mc61@hawaii.edu>
/// @date 25_FEB_2022
///////////////////////////////////////////////////////////////////////////////

#ifndef REPORTCATS_H
#define REPORTCATS_H

#include "catDatabase.h"
#include "validate.h"

void printCat(const size_t index);

void printAllCats();

int findCat(const char *name);

#endif